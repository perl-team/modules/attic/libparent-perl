libparent-perl (0.225-1) UNRELEASED; urgency=low

  [ Jonathan Yu ]
  Only a documentation change
  IGNORE-VERSION: 0.225-1

  * New upstream release

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

 -- Jonathan Yu <jawnsy@cpan.org>  Tue, 08 Mar 2011 22:22:40 -0500

libparent-perl (0.224-1) unstable; urgency=low

  * New upstream release.
  * Drop patch 01-fix_manpage.patch (applied upstream).
  * Make build-dep on perl unversioned.
  * debian/copyright: Refer to "Debian systems" instead of "Debian GNU/Linux
    systems"; refer to /usr/share/common-licenses/GPL-1.
  * Bump Standards-Version to 3.9.1.
  * Update my email address.

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 07 Nov 2010 13:48:29 +0100

libparent-perl (0.223-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release. (Closes: #578818)
    + Test fix for Perl compiled without pre-compiled module
      support
  * Updated gregoa's email address
  * Added myself to Uploaders
  * Use the new short debhelper format (& compat 7)

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Ryan Niebur ]
  * Update jawnsy's email address

  [ Ansgar Burchardt ]
  * Use source format 3.0 (quilt); drop quilt framework.
  * Convert debian/copyright to proposed machine-readable format.
  * Add informative header to patch.
  * Bump Standards-Version to 3.8.4.
  * Add myself to Uploaders.

 -- Ansgar Burchardt <ansgar@43-1.org>  Sat, 24 Apr 2010 22:11:27 +0900

libparent-perl (0.221-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - add /me to Uploaders
    - make build dependency on quilt versioned
  * Refresh 01-fix_manpage.patch.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Wed, 26 Mar 2008 18:54:49 +0100

libparent-perl (0.220-1) unstable; urgency=low

  * New upstream release

 -- Damyan Ivanov <dmn@debian.org>  Tue, 18 Mar 2008 11:45:31 +0200

libparent-perl (0.219-2) unstable; urgency=low

  [ David Paleino ]
  * debian/patches:
    - 01-fix_manpage.patch added to avoid "manpage-has-errors-from-man"
  * debian/rules - now handles quilt patch system
  * debian/control:
    - updated to depend on quilt

  [ Jose Luis Rivas ]
  * debian/control:
   + Standards-Version updated to 3.7.3

  [ Damyan Ivanov ]
  * debian/watch: add v? to filename pattern
  * debian/rules:
    + stop passing detailed directories to $(PERL) Makefile.PL
    + remove usr/lib/perl5 if it is installed by MakeMaker
    + fix target inter-dependencies
  * Put me instead of David in Uploaders

 -- Damyan Ivanov <dmn@debian.org>  Wed, 09 Jan 2008 14:06:40 +0200

libparent-perl (0.219-1) unstable; urgency=low

  * Initial Release. Required by libhtml-display-perl, which is required by
    new upstream release of libwww-mechanize-shell-perl

 -- David Paleino <d.paleino@gmail.com>  Sat, 20 Oct 2007 19:42:25 +0200
